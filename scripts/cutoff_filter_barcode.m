function [DataFiltered,MetaDataFiltered]=cutoff_filter_barcode(Data,MetaData,LookUp,Gene)
%occurence of class-only reads
P1=Data.count(generate_barcode_index(ClassIndex.P1,MetaData.plateindex,MetaData.barcodeindex));
R1=Data.count(generate_barcode_index(ClassIndex.R1,MetaData.rowindex,MetaData.barcodeindex));
C1=Data.count(generate_barcode_index(ClassIndex.C1,MetaData.columnindex,MetaData.barcodeindex));
%cutoff is 95th percentile of read counts
PRC_cutoff=[single(prctile(P1,95)),single(prctile(R1,95)),single(prctile(C1,95))];
%define the index all at once
Keep=(MetaData.plateindex&(Data.count>PRC_cutoff(1)))|(MetaData.rowindex&(Data.count>PRC_cutoff(2)))|(MetaData.columnindex&(Data.count>PRC_cutoff(3)));
%recreate data
DataFiltered.count=Data.count(Keep);
DataFiltered.group=Data.group(Keep);
DataFiltered.barcode=Data.barcode(Keep);
DataFiltered.relative_counts=Data.relative_counts(Keep);
%recreate MetaData
[MetaDataFiltered.group,MetaDataFiltered.index]=order_barcode_groups(DF.group);
[MetaDataFiltered.barcode,MetaDataFiltered.barcodeindex]=order_barcodes(DF.barcode);
[MetaDataFiltered.plateindex,MetaDataFiltered.rowindex,MetaDataFiltered.columnindex]=index_classes(DF.group);
MetaDataFiltered.PRC=count_class(MetaDataFiltered.barcodeindex,MetaDataFiltered.plateindex,MetaDataFiltered.rowindex,MetaDataFiltered.columnindex);
MetaDataFiltered=AddInsertionSite(MetaDataFiltered,LookUp);
MetaDataFiltered=AddGeneInfo(MetaDataFiltered,Gene);