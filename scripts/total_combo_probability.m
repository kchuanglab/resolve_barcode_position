function marginalprobability=total_combo_probability(prob)
marginalprobability=cell(size(prob));
for i=1:numel(prob)
    temp=prod(prod(prob{i},2),3);
    marginalprobability{i}=temp./sum(temp(:));
end
    