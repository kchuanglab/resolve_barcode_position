function Data=calculate_relative_counts(Data)
pools=unique(Data.group);
Data.relative_counts=nan(size(Data.count));
for i=1:numel(pools)
    index=strcmpi(pools{i},Data.group);
    Data.relative_counts(index)=Data.count(index)./sum(Data.count(index));
end