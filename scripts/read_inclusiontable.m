function Data=read_inclusiontable(filename)
fid=fopen(filename,'r');
C=textscan(fid,'%s%f%s','Headerlines',1);
Data.barcode=C{1,1};
Data.count=C{1,2};
Data.group=C{1,3};

