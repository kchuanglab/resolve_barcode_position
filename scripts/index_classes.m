function index=index_classes(group,poolnames)
index=false(numel(group),numel(poolnames));
for i=1:numel(poolnames)
    token=strcat('^',poolnames{i},'\d+$');
    index(:,i)=cellfun(@(x) ~isempty(regexp(x,token, 'once')),group);
end