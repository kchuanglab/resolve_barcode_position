function logratioset=extract_high_quality_ratio(MetaData)
Nratio=numel(MetaData.rationames);
index=find(prod(MetaData.classcount,2)==1);
Ndatapoints=numel(index);
logratioset=nan(Ndatapoints,Nratio);
for i=1:Ndatapoints
    logratioset(i,:)=MetaData.comboratio{index(i)};
end