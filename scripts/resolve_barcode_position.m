function [Table,Library,Data,MetaData]=resolve_barcode_position(CountFile,LookUpFile,GeneFile,OutputFile)
%-------------------------------------------------------------------------!
% barcode_position.m
% -----------------:
% Usage:
% [Table,Library,Data,MetaData]=barcode_position(CounFile,LookUpFile,GeneFile)
%-------------------------------------------------------------------------!
% Given an input file of the read counts in different pools (CountFile),
% the assignment of barcode to insertion site (LookUpFile) and information
% about the genes of the organism (GeneFile), outputs the deconvoluted 
% list of insertion mutants in a sorted library. Currently assumes that the
% pooling strategy is 3-dimensional (Row, Column, Plate).
%-------------------------------------------------------------------------!
% Anthony Shiver (2019) Kerwyn Huang Laboratory of Cellular Organization  :
%-------------------------------------------------------------------------!


%----read in and add information to the barcodes, Data, and MetaData
[MetaData.poolnames,MetaData.rationames,MetaData.ratioindex]=PoolingStrategy('Cartesian');

%load and normalize inclusiontable
Data=read_inclusiontable(CountFile);
Data=calculate_relative_counts(Data);

%generate the lookup table
LookUp=read_lookuptable(LookUpFile,'sortedlibrary');
Gene=read_genetable(GeneFile);
LookUp=gene_info(LookUp,Gene);

%create MetaData
[MetaData.group,MetaData.index]=order_barcode_groups(Data.group,MetaData.poolnames);
[MetaData.barcode,MetaData.barcodeindex]=order_barcodes(Data.barcode);
MetaData.barcodepoolcrossindex=find_barcode_pool_index(MetaData.barcodeindex,MetaData.group,Data.group);

%Add Insertion Site Information
MetaData=insertion_sites(MetaData,LookUp);

%Add Class Information
MetaData.classindex=index_classes(Data.group,MetaData.poolnames);
MetaData.classcount=count_class(MetaData.barcodeindex,MetaData.classindex);

%count the pool inclusions, assign a value to unmappable, unambiguous, and
%ambiguous
MetaData.mappingclass={'unmappable' 'unambiguous' 'probabilistic'};
MetaData.mappinginfo=assign_map_class(MetaData.classcount);

%extract pool information for each barcode
MetaData.poolset=extractpoolset(MetaData.barcodeindex,MetaData.classindex,Data.group,MetaData.poolnames);
MetaData.poolcombo=generate_possible_library_wells(MetaData.poolset,'Cartesian',MetaData.classcount);
[MetaData.comboratio,MetaData.comboreads,MetaData.comboindex]=read_ratio(MetaData.poolcombo,MetaData.group,MetaData.barcodepoolcrossindex,Data.relative_counts,MetaData.poolnames,MetaData.ratioindex);

%Calculate the pdfs for ratios, add that information to MetaData
logratiosets=extract_high_quality_ratio(MetaData);
[RP,CP,RC]=Cartesian_Voigt_PDF(logratiosets);
PDF(1)=RP;PDF(2)=CP;PDF(3)=RC;
MetaData=calculate_combo_probability(MetaData,PDF);
MetaData.marginalprobability=total_combo_probability(MetaData.comboprobability);

%Assign barcodes to the library by probability
Library=cell(40,96);
for i=1:numel(MetaData.barcode)
    if ~MetaData.mappinginfo(i,1)
        Library=probabilistic_barcode_position(Library,MetaData,i);
    end
end

%Renormalize read counts in the library to assign strain abundance per well
Library=relative_abundance(Library);

%resolve sequencing errors
Library=collapse_error(Library,2);

%assign insertion class
Library=assign_insertion_class(Library);

%Create a table from the sorted library
Table=library_to_table(Library);

%Save data
writetable(Table,OutputFile,'Delimiter','\t','FileType','text');
save(OutputFile);
