function Res=Barstow_Voigt_Residual(range,hist,param)
Res=Barstow_Voigt(range,param)-hist;