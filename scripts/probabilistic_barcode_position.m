function Library = probabilistic_barcode_position(Library,MetaData,i)
%if the barcode isn't mapped within the library, ignore
if isempty(MetaData.poolcombo{i})
    return;
end

max_likelihood=MetaData.marginalprobability{i}==max(MetaData.marginalprobability{i});
%if there is a tie for mosty likely position, ignore
if sum(max_likelihood)>1
    return;
end
%convert to plate and vector index
[plate,index]=cartesian_index(MetaData.poolcombo{i}(max_likelihood,:,:),96);
Nwells=numel(plate);

for k=1:Nwells
    Table=library_position_table(MetaData,i,find(max_likelihood),k);
    if isempty(Library{plate(k),index(k)})
        Library{plate(k),index(k)}=Table;
    else
        Library{plate(k),index(k)}=[Library{plate(k),index(k)};Table];
    end
end
    