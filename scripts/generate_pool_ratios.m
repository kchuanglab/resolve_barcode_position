function [PRcount,PCcount,RCcount,PRfrac,PCfrac,RCfrac]=generate_pool_ratios(BarcodeLibrary)
[r,c]=size(BarcodeLibrary);
PRcount=[];
PRfrac=[];
PCcount=[];
PCfrac=[];
RCcount=[];
RCfrac=[];

for i=1:r
    for j=1:c
        [bc,~]=size(BarcodeLibrary{i,j});
        if bc==1
            Token=BarcodeLibrary{i,j};
            PRcount=[PRcount;(double(Token{2}(1))/double(Token{2}(2)))];
            PCcount=[PCcount;(double(Token{2}(1))/double(Token{2}(3)))];
            RCcount=[RCcount;(double(Token{2}(2))/double(Token{2}(3)))];
            PRfrac=[PRfrac;(Token{3}(1)/Token{3}(2))];
            PCfrac=[PCfrac;(Token{3}(1)/Token{3}(3))];
            RCfrac=[RCfrac;(Token{3}(2)/Token{3}(3))];
        end
    end
end
