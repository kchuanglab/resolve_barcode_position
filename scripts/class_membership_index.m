function Stat=class_membership_index(MetaData)
Stat.P1=MetaData.PRC(:,1)==1&MetaData.PRC(:,2)==0&MetaData.PRC(:,3)==0;
Stat.R1=MetaData.PRC(:,1)==0&MetaData.PRC(:,2)==1&MetaData.PRC(:,3)==0;
Stat.C1=MetaData.PRC(:,1)==0&MetaData.PRC(:,2)==0&MetaData.PRC(:,3)==1;
Stat.PR=MetaData.PRC(:,1)==1&MetaData.PRC(:,2)==1&MetaData.PRC(:,3)==0;
Stat.PC=MetaData.PRC(:,1)==1&MetaData.PRC(:,2)==0&MetaData.PRC(:,3)==1;
Stat.RC=MetaData.PRC(:,1)==0&MetaData.PRC(:,2)==1&MetaData.PRC(:,3)==1;
Stat.Unique=MetaData.PRC(:,1)==1&MetaData.PRC(:,2)==1&MetaData.PRC(:,3)==1;
Stat.Double=MetaData.PRC(:,1)==2&MetaData.PRC(:,2)==2&MetaData.PRC(:,3)==2;
Stat.Triple=MetaData.PRC(:,1)==3&MetaData.PRC(:,2)==3&MetaData.PRC(:,3)==3;
Stat.Quadruple=MetaData.PRC(:,1)==4&MetaData.PRC(:,2)==4&MetaData.PRC(:,3)==4;
