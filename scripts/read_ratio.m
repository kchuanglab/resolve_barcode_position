function [cellratio,cellread,cellindex] = read_ratio(poolcombo,group,barcodepoolcrossindex,relative_counts,poolnames,ratioindex)
%step through each barcode, extract its combo]
[bc,~]=size(poolcombo);
[~,Nratio]=size(ratioindex);
Npoolclasses=numel(poolnames);
cellread=cell(bc,1);
cellindex=cell(bc,1);
cellratio=cell(bc,1);
for i=1:bc
    set=poolcombo{i};
    index=nan(size(set));
    read=nan(size(set));
    [Ncombo,Ndim,Nwell]=size(set);
    ratio=nan(Ncombo,Nratio,Nwell);
    %if there are at least as many pools as there should be
    if Ndim==Npoolclasses
        %first fill in an index and read count for every possible combo
        for j=1:Ncombo
            for k=1:Nwell
                for m=1:numel(poolnames)
                    mypool=strcat(poolnames{m},num2str(set(j,m,k)));
                    index(j,m,k)=barcodepoolcrossindex(i,strcmpi(mypool,group));
                    read(j,m,k)=relative_counts(index(j,m,k));
                end
            end
        end
        %second use the read counts to calculate a ratio
        for a=1:Nratio
            index1=ratioindex(1,a);
            index2=ratioindex(2,a);
            ratio(:,a,:)=read(:,index1,:)./read(:,index2,:);
        end
    end
    cellread{i}=read;
    cellindex{i}=index;
    cellratio{i}=log(ratio);
end
    
%for every triplet of the combo, get the combo
