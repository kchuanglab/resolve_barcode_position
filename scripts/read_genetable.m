function Gene=read_genetable(filename)
fid=fopen(filename,'r');
%{
    1. locusId
    2. sysName
    3. scaffoldId
    4. begin
    5. end
    6. strand
    7. type
    8. desc
%}
                   %1 2 3 4 5 6 7 8 
cell=textscan(fid,'%d%s%s%u%u%s%s%s','HeaderLines',1,'Delimiter','\t');

Gene.name=cell{2};
Gene.begin=cell{4};
Gene.end=cell{5};
Gene.strand=cell{6};
Gene.scaffold=cell{3};
Gene.info=cell{8};