function LookUp=gene_info(LookUp,Gene)

Ninsertions=numel(LookUp.rcbarcode);

%Initialize new fields
LookUp.genic=false(Ninsertions,1);
LookUp.inner=false(Ninsertions,1);
LookUp.intergenic=false(Ninsertions,1);
LookUp.name=cell(Ninsertions,1);
LookUp.intergenicgenes=cell(Ninsertions,1);


inneroffset=floor((double(Gene.end)-double(Gene.begin))./10);
for i=1:Ninsertions
    Location=find((LookUp.position(i)>Gene.begin & LookUp.position(i)<Gene.end) & (strcmpi(LookUp.scaffold(i),Gene.scaffold)),1,'first');
    %if Location found, then the insertion is genic
    if(~isempty(Location))
        LookUp.genic(i)=true;
        LookUp.name(i)=Gene.name(Location);
        %if within offset, then the insertion is inner-80%
        if LookUp.position(i)>(Gene.begin(Location)+inneroffset(Location))&&...
           LookUp.position(i)<(Gene.end(Location)-inneroffset(Location))
                LookUp.inner(i)=true;
        end
    %if Location not found, at least report nearby genes
    else
        leftmost=Gene.end<LookUp.position(i) & strcmpi(LookUp.scaffold(i),Gene.scaffold);
        rightmost=Gene.begin>LookUp.position(i) & strcmpi(LookUp.scaffold(i),Gene.scaffold);
        if sum(leftmost)>0 && sum(rightmost)>0
            LT=Gene.end==max(Gene.end(leftmost));
            GT=Gene.begin==min(Gene.begin(rightmost));
            LookUp.intergenicgenes{i}=strcat(Gene.name{LT},'-',Gene.name{GT});
            LookUp.intergenic(i)=true;
        end
    end
end