function [plate,index]=cartesian_index(set,Nwells)
[~,~,Ntemp]=size(set);
set2=set;
set=nan(Ntemp,3);
for i=1:Ntemp
    set(i,:)=set2(1,:,i);
end
for i=1:Ntemp
    plate(i)=set(i,1);
    switch Nwells
        case 96
            index(i)=12*(set(i,2)-1)+set(i,3);
        case 384
            index(i)=24*(set(i,2)-1)+set(i,3);
        otherwise
    end
end