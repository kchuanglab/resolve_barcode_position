function y = Barstow_Voigt(x,param)
a=param(1);
c=param(2);
delta=param(3);
sigma=param(4);

Arg1=(-1i.*(x-c)+delta)/sqrt(2)*sigma;
Arg2=( 1i.*(x-c)+delta)/sqrt(2)*sigma;

z=a*(exp(Arg1.^2).*double(erfc(sym(Arg1)))+exp(Arg2.^2).*double(erfc(sym(Arg2))))/(2*sqrt(2*pi)*sigma);
y=real(z);