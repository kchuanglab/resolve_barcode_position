function label=generate_position_label(Nplate,Nwell)
%-------------------------------------------------------------------------!
% generate_position_label.m
% ------------------------:
% Usage: label=generate_position_label(Nplate,Nwell)
% -------------------------------------------------:
% Generates a cell array of dimensions Nplate x Nwell containing a 
% label for each position in a set of multi-well plates (e.g. P1-C6)
%-------------------------------------------------------------------------!
% Nplate: any integer
% Nwell:  96 | 384
%-------------------------------------------------------------------------!
% Anthony Shiver (2019) Kerwyn C Huang Laboratory of Cellular Organization:
%-------------------------------------------------------------------------!
if ~isaninteger(Nplate) || ~isaninteger(Nwell)
    error('Both arguments passed must be integers')
end
switch Nwell
    case 96
        column=repmat(cellfun(@(x) num2str(x),num2cell((1:12)'),'UniformOutput',false),8,1);
        letters={'A' 'B' 'C' 'D' 'E' 'F' 'G' 'H'};
        rowindex=ones(8,12).*repmat((1:8)',1,12);
        row=letters(rowindex)';
    case 384
        column=repmat(cellfun(@(x) num2str(x),num2cell((1:24)'),'UniformOutput',false),16,1);
        letters={'A' 'B' 'C' 'D' 'E' 'F' 'G' 'H' 'I' 'J' 'K' 'L' 'M' 'N' 'O' 'P'};
        rowindex=ones(16,24).*repmat((1:16)',1,24);
        row=letters(rowindex)';
    otherwise
        error('Number of wells (Nwell) must be either 96 or 384, you specified: %u',Nwell)
end
well=repmat((strcat(row(:),column))',Nplate,1);
plateindex=cellfun(@(x) num2str(x),num2cell(ones(Nplate,Nwell).*repmat((1:Nplate)',1,Nwell)),'UniformOutput',false);
label=strcat('P',plateindex,'-',well);
end

function logical=isaninteger(x)
    logical= isfinite(x) & x==floor(x) & numel(x)==1 & isnumeric(x);
end