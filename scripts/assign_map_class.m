function map_class = assign_map_class(class_count)
[b,~]=size(class_count);
map_class=false(b,3);
class_max=nanmax(class_count,[],2);
class_multiple=prod(class_count,2);
map_class(:,1)=class_multiple==0;
map_class(:,2)=class_max==class_multiple;
map_class(:,3)=class_multiple>class_max;
