function ReducedTable=ResolveSequencingErrors(Table,MaxDist)
[Nrows,~]=size(Table.barcode);
%variants are less than or equal to MaxDist
variants=cell_hamming(Table.barcode,MaxDist);
%equal are distance of zero
equal=cell_hamming(Table.barcode,0);
%the max fractional abundance is the main barcode
topbarcode=Table.fractionalabundance==max(Table.fractionalabundance);
%tag variants of the main barcode
collapse=false(Nrows,1);
compare=find(topbarcode,1,'first');
Fmax=Table.fractionalabundance(compare);
for i=1:Nrows
    if variants(i,compare) && ~equal(i,compare)
        %found a variant tag it
        collapse(i)=true;
    end
end
%read contribution
collapsefractionalabundance=sum(Table.fractionalabundance(collapse));
Table.fractionalabundance(topbarcode)=Fmax+collapsefractionalabundance;
ReducedTable=Table(~collapse,:);
end

function hamming_logical=cell_hamming(cell,maxdist)
%replace DNA code and convert cell to mat
N=numel(cell);
hammingdist=nan(N);
for i=1:N
    for j=1:N
        hammingdist(i,j)=sum(cell{i}~=cell{j});
    end
end
hamming_logical = hammingdist<=maxdist;
end



        