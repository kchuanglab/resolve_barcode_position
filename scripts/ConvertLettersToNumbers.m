function array=ConvertLettersToNumbers(array)
Letter={'A' 'B' 'C' 'D' 'E' 'F' 'G' 'H' 'I' 'J' 'K' 'L' 'M' 'N' 'O' 'P' 'Q'...
        'R' 'S' 'T' 'U' 'V' 'W' 'X' 'Y' 'Z'};
for i=1:numel(Letter)
    token=strcat(Letter{i},'$');
    array=cellfun( @(x) regexprep(x,token,num2str(i)),array,'UniformOutput',false);
end
