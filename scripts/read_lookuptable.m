function LookUp=read_lookuptable(filename,format)
fid=fopen(filename);
switch format
    case 'randompool'
        cell=textscan(fid,'%s%s%u%u%s%s%u%u%s%s%u%u','HeaderLines',1,'Delimiter','\t');
        LookUp.rcbarcode=cell{2};
        LookUp.n1=double(cell{4});
        LookUp.scaffold1=cell{5};
        LookUp.strand1=cell{6};
        LookUp.pos1=cell{7}; LookUp.pos1(LookUp.pos1==0)=NaN;
        LookUp.n2=double(cell{8});
        LookUp.scaffold2=cell{9};
        LookUp.strand2=cell{10};
        LookUp.pos2=cell{11}; LookUp.pos2(LookUp.pos2==0)=NaN;
    case 'sortedlibrary'
        cell=textscan(fid,'%s\t%s\t%u\t%u\t%s\t%u\t%s\t%s\n','HeaderLines',1,'Delimiter','\t');
        LookUp.rcbarcode=cell{2};
        LookUp.ntot=cell{3};
        LookUp.n=cell{4};
        LookUp.scaffold=cell{5};
        LookUp.position=cell{6};
        LookUp.strand=cell{7};
        LookUp.status=cell{8};
        
        
end


