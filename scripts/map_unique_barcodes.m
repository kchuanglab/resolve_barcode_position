 function barcode_map=map_unique_barcodes(Data,MetaData,Unique)
%create an empty cell array
barcode_map=cell(40,96);
%step through the index of unique barcodes
uniq_ind=find(Unique);
for i=1:numel(uniq_ind)
    %extract the group set information
    barcode=MetaData.barcode(uniq_ind(i));
    set=Data.group(MetaData.barcodeindex(uniq_ind(i),:));
    
    relative_count=Data.relative_counts(MetaData.barcodeindex(uniq_ind(i),:));
    count=Data.count(MetaData.barcodeindex(uniq_ind(i),:));
    
    scaffold1=MetaData.scaffold1(uniq_ind(i));
    scaffold2=MetaData.scaffold2(uniq_ind(i));
    
    strand1=MetaData.strand1(uniq_ind(i));
    strand2=MetaData.strand2(uniq_ind(i));
    
    pos1=MetaData.pos1(uniq_ind(i));
    pos2=MetaData.pos2(uniq_ind(i));
    
    n1=MetaData.n1(uniq_ind(i));
    n2=MetaData.n2(uniq_ind(i));
    
    genic=MetaData.Genic(uniq_ind(i));
    inner80=MetaData.Inner80(uniq_ind(i));
    if genic
        genename=MetaData.Name{uniq_ind(i)};
    else
        genename=MetaData.IntergenicGenes{uniq_ind(i)};
    end
    %generate the binary index
    [row,column,triplet_ind]=generate_binary_indices(set);
    %fill the cell array with  barcode and PRC count
    barcode_map{row,column}=[barcode_map{row,column};{barcode,count(triplet_ind),relative_count(triplet_ind),pos1,strand1,scaffold1,n1,pos2,strand2,scaffold2,n2,genic,inner80,genename}];
end
%step through the matrix, and if there are multiple entries, use count info
%to estimate a percentage of the cells.