function Library=relative_abundance(Library)
[p,ind]=size(Library);
for i=1:p
    for j=1:ind
        if ~isempty(Library{i,j})
            barcodes=Library{i,j}.barcode;
            if ~iscell(barcodes)
                barcodes={barcodes};
            end
            counts=[Library{i,j}.P,Library{i,j}.R,Library{i,j}.C];
            %perform calculation by barcode
            [uniquebarcodes,index]=unique(barcodes);
            uniquecounts=counts(index,:);
            frac=uniquecounts./repmat(sum(uniquecounts,1),numel(uniquebarcodes),1);
            fractionalabundance=nanmean(frac,2)./sum(nanmean(frac,2),1);
            %translate back to larger table
            groupindex=grp2idx(barcodes);
            Element.fractionalabundance=fractionalabundance(groupindex);
            Library{i,j}=[Library{i,j},struct2table(Element)];
        end
    end
end