function MetaData=initialize_metadata_barcode(Data,LookUp,Gene)
[MetaData.group,MetaData.index]=order_barcode_groups(Data.group);
[MetaData.barcode,MetaData.barcodeindex]=order_barcodes(Data.barcode);
[MetaData.plateindex,MetaData.rowindex,MetaData.columnindex]=index_classes(Data.group);
MetaData.PRC=count_class(MetaData.barcodeindex,MetaData.plateindex,MetaData.rowindex,MetaData.columnindex);
%Add Insertion Site Information
MetaData=AddInsertionSite(MetaData,LookUp);
%Add Gene Information
MetaData=AddGeneInfo(MetaData,Gene);
