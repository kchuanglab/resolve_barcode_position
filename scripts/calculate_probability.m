function probability=calculate_probability(ratios,PDF)
[a,b,c]=size(ratios);
probability=nan(size(ratios));
for i=1:a
    for j=1:b
        for k=1:c
            probability(i,j,k)=individual_probability(ratios(i,j,k),PDF(j));
        end
    end
end
         
            
    