function [names,rationames,ratioindex] = PoolingStrategy(strategy)
switch strategy
    case 'Cartesian'
        names={'P' 'R' 'C'};
        rationames={'R/P' 'C/P' 'R/C'};
        ratioindex=[[2;1],[3;1],[2;3]];
end