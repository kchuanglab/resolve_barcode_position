function convert=ConvertPriceGroupsToCartesian(initial)
PriceGroupNames={'plate' 'row' 'col'};
CartesianGroupNames={'P' 'R' 'C'};
convert=cell(size(initial));

for i=1:numel(PriceGroupNames)
    token=strcat('^(',PriceGroupNames{i},')(\d{1,3}|[A-Z])');
    group_index=cellfun(@(x) ~isempty(regexp(x,token,'ONCE')),initial);
    
    temp=initial(group_index);
    transform=cellfun(@(x) regexprep(x,PriceGroupNames{i},CartesianGroupNames{i}),temp,'UniformOutput',false);
    convert(group_index)=transform;
end
    
convert=ConvertLettersToNumbers(convert);