function ClassCount=count_class(barcodeindex,classindex)
[b,~]=size(barcodeindex);
[~,c]=size(classindex);
ClassCount=nan(b,c);
for i=1:c
    ClassCount(:,i)=sum(barcodeindex&repmat(classindex(:,i)',b,1),2);
end