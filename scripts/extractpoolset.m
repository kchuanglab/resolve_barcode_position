function [poolset] = extractpoolset(barcodeindex,classindex,pool,poolnames)
Npoolclasses=numel(poolnames);
[Nbarcodes,~]=size(barcodeindex);
poolset=cell(Nbarcodes,Npoolclasses);
for i=1:Nbarcodes
    for k=1:Npoolclasses
        poolstr=pool(barcodeindex(i,:)'&classindex(:,k));
        Token='(?<N>\d+$)';
        Npools=numel(poolstr);
        poolnum=nan(numel(poolstr),1);
        for j=1:Npools
            Match=regexp(poolstr{j},Token,'names');
            if ~isempty(Match)
                poolnum(j)=str2double(Match.N);
            end
        end
        poolset{i,k}=poolnum;
    end
end

% bc x 3 cell, each element of cell has the pools of barcode bc in class i