function p=individual_probability(ratio,structure)
index=find(ratio>structure.x,1,'last');
if isempty(index) && ~isnan(ratio) %if there is no greater x, and ratio is a number
                                    %then ratio is greater than the range,
                                    %use the last value as an approximate
    index=1;
end
p=structure.y(index);
if isempty(p)
    p=NaN;
end