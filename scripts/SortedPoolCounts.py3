#
#------------------------------------------------------------------------------------------!
# SortedPoolCounts.py3                                                                     :
# -------------------:                                                                     :
# Usage: SortedPoolCounts.py3 -k key_file -o output_file [-q min_quality] [-f min_fraction]:
#------------------------------------------------------------------------------------------!
# SortedPoolCounts is a wrapper for the FEBA script MultiCodes.pl that repeatedly calls    :
# the script for each demuxed read file for pools of a sorted library. The script then     :
# appends the pool label and does a simple filtering step to eliminate barcodes with       :
# low read counts from each pool that may be noise.                                        :
# The read count file can be used directly in the scripts that determine the position of   :
# mutants in the sorted library.                                                           :
#------------------------------------------------------------------------------------------!
# Anthony Shiver (2019) KC Huang Laboratory of Cellular Organization                       :
#------------------------------------------------------------------------------------------!
import os
import argparse
import subprocess
import pandas
import re
import numpy

def argument_parser():
	Parser=argparse.ArgumentParser(	prog='SortedPoolCounts.py3',
									description='''\
									Combines demuxed fastq files to calculate total counts of barcodes
									from BarSeq in different pools from a sorted library.

									The key_file must be tab delimited with the fields <<file path, index name, pool name>>. A header line is assumed.
									'file path' is the path to the fastq file
									'index name' is the appropriate index for this sample, index name is used to look up an index sequence from a lookup table internal to the FEBA set of scripts
									'pool name' is the name that you want to use to label this pool, e.g. P1 for plate 1. For 3-D data, use P# for plates, R# for rows, and C# for columns. Currently,
									this script assumes that the data is in a 3-D format [P,R,C].

									If the fastq files are still gzipped, the script will temporarily decompress them and pass the uncompressed file to FEBA scripts. 


									''',
									epilog='''\
									Writen by Anthony Shiver (Kerwyn Huang Laboratory of Cellular Organization)
									''')

	Parser.add_argument('-k','--key_file',
						type=argparse.FileType('r'),
						required=False,
						metavar='KeyFile',
						dest='KeyFile')
	Parser.add_argument('-o','--output',
						type=argparse.FileType('w'),
						required=True,
						metavar='OutputFile',
						dest='OutputFile')
	Parser.add_argument('-q','--Qmin',
						type=int,
						required=False,
						default=10,
						metavar='MinumumQualityScore',
						dest='Qmin')
	Parser.add_argument('-f','--Fmin',
						type=float,
						required=False,
						default=0.01,
						metavar='MinimumFractionRetained',
						dest='Fmin')
	#in case you want to adjust the -f values
	#without the overhead of reading in the fastq files
	Parser.add_argument('-jf','--JustFilter',
						action='store_true',
						dest='justfilter')
	Parser.add_argument('-i','--input',
						type=argparse.FileType('r'),
						required=False,
						metavar='InputForFiltering',
						dest='input')
	return(Parser)

def read_barcode_count(File,Pool):
	Counts=pandas.read_csv(File,
						   header=1,
					   	   sep='\s+',
					   	   engine='python',
					   	   usecols=[0,1],
					   	   names=['BarcodeSequence','Count'],
					   	   dtype={'BarcodeSequence':str,'Count':int})
	Counts['Pool']=Pool
	return Counts

def read_barcode_dataframe(File):
	Counts=pandas.read_csv(File,
						   header=1,
					   	   sep='\s+',
					   	   engine='python',
					   	   usecols=[0,1,2],
					   	   names=['BarcodeSequence','Count','Pool'],
					   	   dtype={'BarcodeSequence':str,'Count':int,'Pool':str})
	return Counts

def filter_read_counts(DataFrame,Fmin):
# 0: BarcodeSequence 1: Count 2: Pool
# First infers the expected number of barcodes for each class from the list
# then steps through each pool and applies that cutoff, using Fmin as well
	UniquePools=set(DataFrame.Pool)
	ExpectedBarcodes=infer_barcode_number(UniquePools)
	FilteredDataFrame=pandas.DataFrame();
	poolre=re.compile('(?P<class>\D+)(?P<number>\d+)')
	for Pool in UniquePools:
		poolmatch=poolre.search(Pool)
		Cutoff=ExpectedBarcodes[poolmatch.group('class')]
		MiniFrame=DataFrame.loc[DataFrame['Pool']==Pool].sort_values('Count',ascending=False)
		PoolAverage=MiniFrame['Count'].iloc[0:Cutoff].median();
		MiniFilteredDataFrame=MiniFrame.loc[MiniFrame['Count']>=(Fmin*PoolAverage)]
		FilteredDataFrame=FilteredDataFrame.append(MiniFilteredDataFrame)

	FilteredDataFrame=FilteredDataFrame.sort_values('Pool')
	return FilteredDataFrame

def infer_barcode_number(poolnames):
#currently assumes that the pool design is 3D, with
#P for plate, R for row, and C for column
#find 
	platenumbers=[int(pool.replace('P','')) for pool in poolnames if 'P' in pool]
	Pcount=int(max(platenumbers))
	Expectation=dict()
	Expectation['P']=int(96/2 - 1)
	Expectation['R']=int(Pcount*(12/2) - 1)
	Expectation['C']=int(Pcount*(8/2) - 1 )
	return Expectation



#for every unique pool

#count the total number of reads

#expectation plate: 96, column: 8*40 , row: 12*40

def main():
	Parser=argument_parser()
	Arguments=Parser.parse_args()
	DataFrame=pandas.DataFrame()
	if(Arguments.justfilter):
		DataFrame=read_barcode_dataframe(Arguments.input)

	else:
		FQzip=re.compile('fastq\.gz$')
		FQ=re.compile('fastq$')
		if not os.path.exists('poolcountstemp'):
			os.mkdir('poolcountstemp')
		#skip header line
		next(Arguments.KeyFile)
		#step through each fastq file and find the counts
		for Line in Arguments.KeyFile:
			(FastQ,Index,Pool)=Line.rstrip('\n').split('\t')
			TempIndex='poolcountstemp/'+Index
			if FQzip.search(FastQ):
				TempQ='poolcountstemp/'+os.path.basename(FastQ)
				TempQ=re.sub('\.gz$','',TempQ)
				os.system(f"gunzip -c {FastQ} > {TempQ}")
				os.system(f"MultiCodes.pl -bs3 -index {Index} -out {TempIndex} -minQuality {Arguments.Qmin} < {TempQ} &> {TempIndex}.log")
				os.system(f"rm {TempQ}")
			elif FQ.search(FastQ):
				subprocess.run(f"MultiCodes.pl -bs3 -index {Index} -out {TempIndex} -minQuality {Arguments.Qmin} < {FastQ} &> {TempIndex}.log")
			else:
				die("FastQ is not a recognized format.")
			CountFile=TempIndex+'.codes'
			PoolDataFrame=read_barcode_count(CountFile,Pool)
			DataFrame=DataFrame.append(PoolDataFrame)
			os.system(f"rm {TempIndex}.counts {TempIndex}.codes {TempIndex}.close")

	DataFrame=filter_read_counts(DataFrame,Arguments.Fmin)
	DataFrame.to_csv(Arguments.OutputFile,sep='\t',index=False)


main()

