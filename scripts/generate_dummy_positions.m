DummyCombination=cell(6,6,6);
for i=6:6
    for j=1:6
        for k=1:6
            temp=cartesian_position_generator({(1:i) (1:j) (1:k)});
            DummyCombination{i,j,k}=eliminate_equivalent_combos(temp);
            temp=[];
        end
    end
end