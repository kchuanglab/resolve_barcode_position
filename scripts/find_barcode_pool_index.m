function barcode_pool_index = find_barcode_pool_index(barcodeindex,uniquegroup,datagroup)
[bc,~]=size(barcodeindex);
barcode_pool_index=nan(bc,numel(uniquegroup));

for j=1:numel(uniquegroup)
    grpind=strcmpi(uniquegroup{j},datagroup);
    for i=1:bc
        index=find(grpind&barcodeindex(i,:)',1,'first');
        if ~isempty(index)
            barcode_pool_index(i,j)=index;
        end
    end
end
        
        