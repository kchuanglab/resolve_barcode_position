function [ordered_groups,index]=order_barcode_groups(group,poolnames)

% for every group name in poolnames, find the max value
poolmax=nan(size(poolnames)); %same dimensions ans poolnames
for i=1:numel(poolnames)
    Token=strcat('^',poolnames{i},'(?<N>\d+)$');
    Nmax=0;
    for k=1:numel(group)
        MyMatch=regexp(group{k},Token,'names');
        if ~isempty(MyMatch)
            Nmax=nanmax([Nmax;str2double(MyMatch.N)]);
        end
    end
    poolmax(i)=Nmax;
end
        
% for every group, create ordered set with 1:max
SetHolder=cell(size(poolnames));
for i=1:numel(poolnames)
    I=cell(poolmax(i),1);
    for j=1:poolmax(i);
        I{j}=num2str(j);
    end
    SetHolder{i}=strcat(poolnames{i},I);
end

% put the ordered groups back together as a single vector
ordered_groups=[];
for i=1:numel(poolnames)
    ordered_groups=[ordered_groups;SetHolder{i}];
end

%create the index of each group in the data
index=false([numel(ordered_groups),numel(group)]);
for i=1:60
    index(i,:)=strcmpi(ordered_groups{i},group');
end
