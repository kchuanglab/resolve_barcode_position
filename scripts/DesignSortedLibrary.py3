# Step through the mapped position file and accumulate counts
# create a hash of hashes, where the barcode is a key for the position which is a key for the count.
# the count is an array, the first element is all counts, the second element is counts that uniq or have a qBeg of 1

import argparse
import pandas
import numpy
from Bio.Seq import Seq
from Bio.Alphabet import generic_dna

def sorted_library_parser():
	Parser=argparse.ArgumentParser(	prog='DesignSortedLibrary.py3',
									description='''\
												Collects information from mapped reads to predict the 
									 			occurence of transposon insertions in the mutant library.
									 			Differs from DesignRandomPool.pl in that multiple instances
									 			the same barcode at different sites are all caught and included
									 			rather than excluded from the list of insertions.

									 			The input files must be delimited with fields
								 				read,barcode,scaffold,pos,strand,uniq,qBeg,qEnd,score,identity
								 				where qBeg and qEnd are the positions in the read, after the 
								 				transposon is removed, that match the target genome.

								 				The gene table must be delimited with the fields
								 				scaffoldId,begin,end,strand,desc\
									 			''',
						 			epilog='''\
						 					Written by Anthony Shiver (Kerwyn Huang Laboratory of Cellular Organization) (2019)
					 						Mostly copied from the perl script DesignRandomPool.pl by Morgan Price.\
					 						'''
									)

	Parser.add_argument('-i','--input',
						type=argparse.FileType('r'),
						required=True,
						nargs='+',
						metavar='InputFileList',
						dest='InputFileList')

	Parser.add_argument('-g','--genes',
						type=argparse.FileType('r'),
						required=True,
						metavar='GeneTable',
						dest='GeneTable')

	Parser.add_argument('-o','--output',
						type=argparse.FileType('w'),
						required=True,
						metavar='OutputFile',
						dest='OutputFile')

	Parser.add_argument('-n','--Nmin',
						type=int,
						required=False,
						default=5,
						metavar='MinimumGoodReads',
						dest='Nmingood')
	#uncomment when ready to implement the random pool mode
	#Parser.add_argument('-f','--fractop',
	#					type=float,
	#					required=False,
	#					default=0.75,
	#					metavar='FractionTopInsertion',
	#					dest='Ftop')

	#Parser.add_argument('-r','--ratiotop',
	#					type=float,
	#					required=False,
	#					default=8.0,
	#					metavar='RatioTopInsertion',
	#					dest='Rtop')

	Parser.add_argument('-q','--qbeg',
						type=int,
						required=False,
						default=3,
						metavar='MaximumQBeg',
						dest='MaxQBeg')

	Parser.add_argument('-p','--randompool',
						action='store_true',
						dest='RandomPoolMode')
	return(Parser)

def collate_insertion_sites(Arguments):
	FrameList=list()
	for FileLocation in Arguments.InputFileList:
		FrameList.append(gather_run(FileLocation,Arguments.MaxQBeg))

	Reads=pandas.concat(FrameList,axis=0,ignore_index=True)
	Raw=group_run(Reads)
	return Raw

def gather_run(Handle,MaxQBeg):
	Reads=pandas.read_csv(Handle,
						  header=None,
						  sep='\t',
						  usecols=[1,2,3,4,5,6],
						  names=['BarcodeSequence','Scaffold','Position','Strand','Unique','qBeg'],
						  dtype={'BarcodeSequence':str,'Scaffold':str,'Position':str,'Strand':str,'Unique':str,'qBeg':str})
	#process what was read in
	#missing values
	MissingValues=Reads.isnull()
	#data type
	Reads.Unique=Reads.Unique.astype('bool')
	Reads.loc[list(MissingValues.Unique),'Unique']=False
	Reads.loc[list(MissingValues.qBeg),'qBeg']='0'
	Reads.qBeg=Reads.qBeg.astype('int')
	#replace elements
	Reads.loc[list(MissingValues.Position),'Position']='*'
	Reads.loc[list(MissingValues.Strand),'Strand']='*'
	#add the computed "good read"
	Reads['GoodRead']=numpy.logical_and(Reads['Unique'].values,numpy.logical_and(Reads['qBeg'].values>0,Reads['qBeg'].values<=MaxQBeg))
	Reads.GoodRead=Reads.GoodRead.astype('int')
	return Reads

def group_run(Reads):
	InsertionGroups=Reads.groupby(['BarcodeSequence','Scaffold','Position','Strand'])
	Raw=pandas.concat([InsertionGroups.size(),InsertionGroups['GoodRead'].sum()],axis=1)
	Raw.reset_index(inplace=True)
	Raw.columns=['BarcodeSequence','Scaffold','Position','Strand','Nall','Ngood']
	return Raw

def pooled_library_format(Raw,OutputFile):
	#Step Through the BarcodePositionCount dictionary, select the usable insertions and store their information.
	CategoryCount=defaultdict(int)
	UsableReadCount=0
	F1=0
	F2=0
	for BarcodeSequence,SiteCount in Raw.items():
		BarcodeObject=Seq(BarcodeSequence,generic_dna)
		#Calculate Ntot
		Ntot=SiteCount.iloc[0,:].sum()
		Npastend=SiteCount.loc['Nall','PastEnd']
		#Remove PastEnd from the dataframe
		SiteCount.drop('PastEnd',axis=1)
		#calculate F1,F2
		if Ntot==1:
			F1+=1
		elif Ntot==2:
			F2+=1

		Nmax=SiteCount.iloc[0,0]
		Nsecond=SiteCount.iloc[0,1]
		Nmax_hq=SiteCount.iloc[1,0] #good barcode count of the most numerous site

		Pmax=list(SiteCount)[0]
		Psecond=list(SiteCount)[1]

		if(Npastend>=Ntot/2 or Npastend>Nmax):
			CategoryCount["PastEnd"]+=1
			N2= Nmax or 0
			#label the insertion site as a "PastEnd" site, this transposon has stuck around without inserting
			print('\t'.join(str(BarcodeObject),str(BarcodeObject.reversecomplement()),Ntot,Npastend,'pastEnd','','',N2,'','',''),
				  file=OutputFile)
			next

		if(Nmax<Ngood or Nmax/Ntot<Ftop):
			CategoryCount["NoCons"]+=1
			next

		if(Nmax_hq<Ngood or Nmax_hq/Ntot<Ftop):
			CategoryCount["FewGood"]+=1
			next

		if(Nmax<Rtop*Nsecond):
			CategoryCount["LoRatio"]+=1
			next

		IncludeBarcode[str(BarcodeObject)]=[Ntot,Nmax,Pmax,Nsecond,Psecond]
		CategoryCount["Usable"]+=1
		UsableReadCount+=Ntot

	
	NonVariantBarcodes=filter_out_variants(IncludeBarcode)

	return NonVariantBarcodes

def sorted_library_format(Raw,Nmingood):
	#create the output dataframe
	AccumulateList=[]
	BarcodeGroup=Raw.groupby('BarcodeSequence')
	for BarcodeSequence,Sites in BarcodeGroup:
		BarcodeObject=Seq(BarcodeSequence,generic_dna)
		Npastend=Sites.loc[Sites.Scaffold.values=='pastEnd','Nall'].sum()
		Ntot=Sites.Nall.sum()
		Ntot_nopastend=Sites.loc[Sites.Scaffold.values!='pastEnd','Nall'].sum()
		Ntop=Sites.loc[Sites.Scaffold.values!='pastEnd','Nall'].max()
		#first if the barcode is mostly pastend, record it as a past end
		if( (Npastend>=Ntot/2 or Npastend>Ntop) and Npastend>Nmingood ):
			Status='PastEnd'
			Sites.Scaffold.values=='pastEnd'
			AccumulateList.append({'barcode':str(BarcodeObject),
								   'rcbarcode':str(BarcodeObject.reverse_complement()),
								   'Ntot':Ntot,
								   'N':Npastend,
								   'Scaffold':'',
								   'Position':'',
								   'Strand':'',
								   'Status':Status})
		#otherwise, only save the barcodes with enough reads
		elif(Ntop>Nmingood):
			TopIndex=Sites.loc[:,'Nall']>=(Ntop/4)
			Nsites=sum(TopIndex)
			if Nsites==1:
				Status='SingleInsertion'
			else:
				Status='AmbiguousInsertions'

			Ftogether=Sites.loc[TopIndex,'Nall'].sum()/Ntot_nopastend
			#the top sites have good stats, save them all
			if(Ftogether>0.75 and (Ntop/Ntot_nopastend)>=(1/Nsites)*.8):
				TopRecord=Sites.loc[TopIndex,:].to_dict('records')
				for row in TopRecord:
					AccumulateList.append({	'barcode':str(BarcodeObject),
								   			'rcbarcode':str(BarcodeObject.reverse_complement()),
								   			'Ntot':Ntot,
								   			'N':row['Nall'],
								   			'Scaffold':row['Scaffold'],
								   			'Position':row['Position'],
								   			'Strand':row['Strand'],
								   			'Status':Status})
			else:
				Status='WeakDistribution'
				FirstIndex=Sites.Nall==Sites.Nall.max()
				AccumulateList.append({		'barcode':str(BarcodeObject),
								   			'rcbarcode':str(BarcodeObject.reverse_complement()),
								   			'Ntot':Ntot,
								   			'N':Sites.loc[FirstIndex,'Nall'].values[0],
								   			'Scaffold':Sites.loc[FirstIndex,'Scaffold'].values[0],
								   			'Position':Sites.loc[FirstIndex,'Position'].values[0],
								   			'Strand':Sites.loc[FirstIndex,'Strand'].values[0],
								   			'Status':Status})


	BarcodeDataFrame=pandas.DataFrame.from_dict(AccumulateList)
	FilteredBarcodes=filter_out_variants(BarcodeDataFrame)
	FilteredBarcodes=FilteredBarcodes[['barcode','rcbarcode','Ntot','N','Scaffold','Position','Strand','Status']]
	return FilteredBarcodes

def print_pool(NonVariantBarcode,OutputFile):
	#Print the remaining barcodes
	for barcode,info in NonVariantBarcode:
		BarcodeObject=Seq(barcode,generic_dna)
		PrintArray=(str(BarcodeObject),str(BarcodeObject.reverse_complement()),Info[0],Info[1],':'.split(Info[2]),Info[3],':'.split(Info[4]))
		print('\t'.PrintArray,file=OutputFile)

def print_library(BarcodeDataFrame,OutputFile):
	BarcodeDataFrame.to_csv(OutputFile,
							sep='\t',
							index=False)

def hamming_distance(seq1,seq2):
	return sum(base1!=base2 for base1,base2 in zip(seq1,seq2))

def test_variant(DataFrame,barcode1,barcode2):
	Set1=set([tuple(x) for x in DataFrame.loc[DataFrame.barcode==barcode1,['Scaffold','Position','Strand']].values])
	Set2=set([tuple(x) for x in DataFrame.loc[DataFrame.barcode==barcode2,['Scaffold','Position','Strand']].values])
	N1=DataFrame.loc[DataFrame.barcode==barcode1,'Ntot'].max()
	N2=DataFrame.loc[DataFrame.barcode==barcode2,'Ntot'].max()
	#If there is any overlap in the set of positions between BC 1 and 2, and the number of reads of BC2 is greater
	#then BC1 is a variant of BC2
	return  ((not Set1.isdisjoint(Set2)) and N2>N1)

def filter_out_variants(BarcodeDataFrame):
	#Step through the good barcodes and only print them if they aren't a variant of another barcode
	MaxHammingDistance=2
	BarcodeArray=list(set(BarcodeDataFrame.barcode.values)) # array of the barcodes
	Nbarcode=len(BarcodeArray)
	HammingDistance=numpy.full((Nbarcode,Nbarcode),-1)					
	
	#Takes lots of time, compute on half the pairwise combinations and then transpose for full matrix
	for i in range(0,Nbarcode-2):
		for j in range(i+1,Nbarcode-1):
			HammingDistance[i,j]=hamming_distance(BarcodeArray[i],BarcodeArray[j])

	DistanceBool= numpy.greater(HammingDistance,0) & numpy.less_equal(HammingDistance,MaxHammingDistance)
	DistanceBoolT= DistanceBool.transpose()
	DistanceBool=DistanceBool|DistanceBoolT #create a symmetric boolean matrix [i,j]==[j,i]

	VariantBarcodes=list()
	CheckLogical=DistanceBool.sum(axis=1)>0
	for i in range(0,Nbarcode-1):
		if CheckLogical[i]: #only check if there is a potential variant in the list
			for j in range(0,Nbarcode-1):
				if DistanceBool[i,j]:
					if test_variant(BarcodeDataFrame,BarcodeArray[i],BarcodeArray[j]):
						VariantBarcodes.append(BarcodeArray[i])
						next
	KeepBool=list()
	for x in BarcodeDataFrame.barcode.values:
		KeepBool.append(not(x in VariantBarcodes))

	FilteredDataFrame=BarcodeDataFrame.loc[KeepBool,:]
	return FilteredDataFrame

def main():
	SortedParser=sorted_library_parser()
	Arguments=SortedParser.parse_args()
	Raw=collate_insertion_sites(Arguments)
	#Print Stuff
	if(Arguments.RandomPoolMode):
		print_pool(pooled_library_format(Raw))
	else:
		print_library(sorted_library_format(Raw,Arguments.Nmingood),Arguments.OutputFile)

main()