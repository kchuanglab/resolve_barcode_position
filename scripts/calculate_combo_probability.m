function MetaData = calculate_combo_probability(MetaData,PDF)
MetaData.comboprobability=cell(numel(MetaData.barcode),1);
for i=1:numel(MetaData.barcode)
    MetaData.comboprobability{i}=calculate_probability(MetaData.comboratio{i},PDF);
end