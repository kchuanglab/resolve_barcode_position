function Table=library_position_table(MetaData,i,j,k)
if isempty(j)
    j=1;
end
if isempty(k)
    k=1;
end
Ninsertions=MetaData.insertioncount(i);
RowCount=max(Ninsertions,1);

Pos.barcode=repmat(MetaData.barcode(i),RowCount,1); 
Pos.P=repmat(MetaData.comboreads{i}(j,1,k),RowCount,1);
Pos.R=repmat(MetaData.comboreads{i}(j,2,k),RowCount,1);
Pos.C=repmat(MetaData.comboreads{i}(j,3,k),RowCount,1);
Pos.logRP=repmat(MetaData.comboratio{i}(j,1,k),RowCount,1);
Pos.logCP=repmat(MetaData.comboratio{i}(j,2,k),RowCount,1);
Pos.logRC=repmat(MetaData.comboratio{i}(j,3,k),RowCount,1);
Pos.marginal=repmat(MetaData.marginalprobability{i}(j),RowCount,1);
Pos.insertioncount=repmat(MetaData.insertioncount(i),RowCount,1); 

if Ninsertions>0
    Pos.contig=num2cell(MetaData.scaffold{i});
    Pos.position=num2cell(MetaData.position{i}); 
    Pos.strand=num2cell(MetaData.strand{i}); 
    Pos.genic=num2cell(MetaData.genic{i}); 
    Pos.inner=num2cell(MetaData.inner{i});
else
    Pos.contig={[]};
    Pos.position={[]};
    Pos.strand={[]};
    Pos.genic={[]};
    Pos.inner={[]};
end
%generate name
Pos.name=cell(RowCount,1);
if Ninsertions>0
    for l=1:Ninsertions
        if Pos.genic{l}
            Pos.name{l}=MetaData.name{i}{l};
        elseif MetaData.intergenic{i}(l)
            Pos.name{l}=MetaData.intergenicgenes{i}{l};
        else
            Pos.name{l}=[];
        end
    end
else
    Pos.name{1}=[];
end
Table=struct2table(Pos);
%reorder table columns
Table=Table(:,{ 'barcode',...
                'contig',...
                'position',...
                'strand',...
                'name',...
                'genic',...
                'inner',...
                'insertioncount',...
                'P',...
                'R',...
                'C',...
                'logRP',...
                'logCP',...
                'logRC',...
                'marginal'});
                