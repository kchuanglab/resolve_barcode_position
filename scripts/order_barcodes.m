function [uniquebarcodes,barcodeindex]=order_barcodes(barcodes)
uniquebarcodes=unique(barcodes);
barcodeindex=false(numel(uniquebarcodes),numel(barcodes));
for i=1:numel(uniquebarcodes)
    barcodeindex(i,:)=strcmpi(uniquebarcodes{i},barcodes);
end

