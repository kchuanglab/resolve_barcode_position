function global_permutation = cartesian_position_generator(full_cell)
%-------------------------------------------------------------------------!
% cartesian_position_generator.m
% -----------------------------:
% Usage: global_permutation = cartesion_position_generator(full_cell)
%-------------------------------------------------------------------------!
% Recursive algorithm that generates all permutations of the passed pools 
%-------------------------------------------------------------------------!
% Anthony Shiver (2019) Kerwyn C. Huang Laboratory of Cellular Organization
%-------------------------------------------------------------------------!


[P,R,C]=full_cell{1,:};
global_permutation=[];

Np=numel(P);
Nr=numel(R);
Nc=numel(C);

if max([Np;Nr;Nc])==1
    global_permutation=[P,R,C];
    return;
else
    
if isempty(P)||isempty(R)||isempty(C)
    global_permutation=[];
    return;
end
    
for p=1:Np
    for r=1:Nr
        for c=1:Nc
            %create the local position index
            local_vector=[P(p),R(r),C(c)];
            %remove the local position index from the global set
            P_remainder=P;
            if Np>1
                P_remainder(p)=[];
            end
            
            R_remainder=R;
            if Nr>1
                R_remainder(r)=[];
            end
            
            C_remainder=C;
            if Nc>1
                C_remainder(c)=[];
            end
            
            cell_remainder={P_remainder,R_remainder,C_remainder};
            %generate permutations of remaining set
            remainder_permutation=cartesian_position_generator(cell_remainder);
            
            [i,j,k]=size(remainder_permutation);
            local_permutation=nan(i,j,k+1);
            local_permutation(:,:,1)=repmat(local_vector,i,1,1);
            local_permutation(:,:,2:end)=remainder_permutation;
            global_permutation=[global_permutation;local_permutation];
        end
    end
end
return;

end