function wellcombos=generate_possible_library_wells(poolset,Strategy,counts)
%-------------------------------------------------------------------------!
% generate_possible_library_wells.m
% --------------------------------:
% Usage:
% wellcombos=generate_possible_library_wells(poolset,Strategy,counts)
%-------------------------------------------------------------------------!
%
%-------------------------------------------------------------------------!
% Anthony Shiver (2019) Kerwyn Huang Laboratory of Cellular Organization  :
%-------------------------------------------------------------------------!

[bc,~]=size(poolset);
wellcombos=cell(bc,1);
switch Strategy
    case 'Cartesian'
        % only consider the barcode if it has less than five instances in 
        % any of the pools.
        % otherwise, generate all possible combinations and then
        % filter so that equivalent combinations are collapsed to one.
        for i=1:bc
            if nanmax(counts(i,:),2)<5
                unfiltered=cartesian_position_generator(poolset(i,:));
                wellcombos{i}=eliminate_equivalent_combos(unfiltered);
                [bc,i]
            end
        end
end