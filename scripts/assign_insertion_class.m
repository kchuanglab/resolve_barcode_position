function Library=assign_insertion_class(Library)
[R,C]=size(Library);
for i=1:R
    for j=1:C
        if ~isempty(Library{i,j})
            Table=Library{i,j};
            [~,ia,ib]=unique(Table.barcode);
            counts=accumarray(ib(:),1,[],@sum);
            definite=Table.marginal(ia)==1;
            insertiontype=cell(numel(counts),1);
            insertiontype(counts==1)={'single'};
            insertiontype(counts>1&definite)={'multiple'};
            insertiontype(counts>1&~definite)={'ambiguous'};
            Table.insertiontype=insertiontype(ib);
            Library{i,j}=Table;
        end
    end
end