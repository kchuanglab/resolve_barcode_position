function [RP,CP,RC]=Cartesian_Voigt_PDF(logratiosets)


row_plate=logratiosets(:,1);
column_plate=logratiosets(:,2);
row_column=logratiosets(:,3);

xrange=-10:0.01:10;
hist_rc=hist(row_column(:),xrange);
hist_rp=hist(row_plate(:),xrange);
hist_cp=hist(column_plate(:),xrange);

% define reasonable range in which to fit the voigt function
range_rc=[851,1051];    % -1.5  : 0.5
range_rp=[701,1051];    % -3    : 0.5
range_cp=[776,1026];    % -2.25 : 0.25

% fitting x and y
xrc=xrange(range_rc(1):range_rc(2));
hrc=hist_rc(range_rc(1):range_rc(2));
xrp=xrange(range_rp(1):range_rp(2));
hrp=hist_rp(range_rp(1):range_rp(2));
xcp=xrange(range_cp(1):range_cp(2));
hcp=hist_cp(range_cp(1):range_cp(2));

%initial parameter
p0_rc=[2976.7,-0.396,0.0457,10.0261];   %stable solution
p0_rp=[712.9397,-1.7054,0.1551,4.7597]; %stable solution
p0_cp=[926.8987,-1.3214,0.1808,5.3348]; %stable solution

%fit the parameters
fun_rc=@(myp) Barstow_Voigt_Residual(xrc,hrc,myp);
fun_rp=@(myp) Barstow_Voigt_Residual(xrp,hrp,myp);
fun_cp=@(myp) Barstow_Voigt_Residual(xcp,hcp,myp);

pfit_rc=lsqnonlin(fun_rc,p0_rc);
pfit_rp=lsqnonlin(fun_rp,p0_rp);
pfit_cp=lsqnonlin(fun_cp,p0_cp);

%create the pdfs
yrc=Barstow_Voigt(xrc,pfit_rc);
pdf_rc=yrc./sum(yrc(:));

yrp=Barstow_Voigt(xrp,pfit_rp);
pdf_rp=yrp./sum(yrp(:));

ycp=Barstow_Voigt(xcp,pfit_cp);
pdf_cp=ycp./sum(ycp(:));

RC.x=xrc;
RC.y=pdf_rc;

RP.x=xrp;
RP.y=pdf_rp;

CP.x=xcp;
CP.y=pdf_cp;
