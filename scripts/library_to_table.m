function Table=library_to_table(library)
%-------------------------------------------------------------------------!
% library_to_table.m
% ------------------------:
% Usage: Table=library_to_table(library)
% -----------------------------------------------------:
% Take the Cell Array format of the library, convert into a table
% format.
%-------------------------------------------------------------------------!
% library:  the cell array format of the sorted library.
%-------------------------------------------------------------------------!
% Anthony Shiver (2019) Kerwyn C Huang Laboratory of Cellular Organization:
%-------------------------------------------------------------------------!

%initialize column names from the first non-empty well, plus the "well"
%column
ColumnNames=library{find(cellfun(@(x) ~isempty(x),library),1,'first')}.Properties.VariableNames;
Table=cell2table(cell(0,numel(ColumnNames)+1),'VariableNames',[{'well'},ColumnNames]);
%Fill in the Table by append the tables from each well, along with a "well"
%label.
[Nplate,indexsize]=size(library);
label=generate_position_label(Nplate,indexsize);
for i=1:Nplate
    for j=1:indexsize
        if ~isempty(library{i,j})
            Element=library{i,j};
            [rows,~]=size(Element);
            Well.well=repmat(label(i,j),rows,1);
            Element=[struct2table(Well),Element];
            Table=[Table;Element];
        end
    end
end
end



        