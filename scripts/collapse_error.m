function Library=collapse_error(Library,MaxDist)
[row,ind]=size(Library);
for i=1:row
    for j=1:ind
        if ~isempty(Library{i,j})
            Library{i,j}=ResolveSequencingErrors(Library{i,j},MaxDist);
        end
    end
end