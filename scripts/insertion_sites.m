function MetaData=insertion_sites(MetaData,LookUp)
n=numel(MetaData.barcode);

MetaData.ntot=cell(n,1);
MetaData.n=cell(n,1);
MetaData.scaffold=cell(n,1);
MetaData.position=cell(n,1);
MetaData.strand=cell(n,1);
MetaData.status=cell(n,1);
MetaData.genic=cell(n,1);
MetaData.inner=cell(n,1);
MetaData.intergenic=cell(n,1);
MetaData.name=cell(n,1);
MetaData.intergenicgenes=cell(n,1);
MetaData.insertioncount=nan(n,1);

for i=1:n
    index=strcmpi(MetaData.barcode{i},LookUp.rcbarcode);
    MetaData.insertioncount(i)=sum(index);
    if sum(index)>0
        MetaData.ntot{i}=LookUp.ntot(index);
        MetaData.n{i}=LookUp.n(index);
        MetaData.scaffold{i}=LookUp.scaffold(index);
        MetaData.position{i}=LookUp.position(index);
        MetaData.strand{i}=LookUp.strand(index);
        MetaData.status{i}=LookUp.status(index);
        MetaData.genic{i}=LookUp.genic(index);
        MetaData.inner{i}=LookUp.inner(index);
        MetaData.intergenic{i}=LookUp.intergenic(index);
        MetaData.name{i}=LookUp.name(index);
        MetaData.intergenicgenes{i}=LookUp.intergenicgenes(index);
    end
    
end
MetaData.insertionmapped=MetaData.insertioncount>0;