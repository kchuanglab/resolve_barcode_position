function ind=generate_barcode_index(row_index,column_index,barcodeindex)
index=barcodeindex;
index(~row_index,:)=false;
index(:,~column_index)=false;
ind=sum(index,1)>0;
