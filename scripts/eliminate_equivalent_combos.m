function filtered = eliminate_equivalent_combos(unfiltered)
if isempty(unfiltered)
    filtered=[];
    return;
end
[Ncombo,~,~]=size(unfiltered);
equivalence=false(Ncombo);
for i=2:Ncombo
    for j=1:i-1
        k=i-j;
        A=squeeze(unfiltered(i,:,:))';
        B=squeeze(unfiltered(k,:,:))';
        equivalence(i,k)=isempty(setdiff(A,B,'rows'));
        if equivalence(i,k)
            break;
        end
    end
end
equivalentfound=sum(equivalence,2)>0;
filtered=unfiltered(~equivalentfound,:,:);