function [Plate,Row,Column]=break_up_counts(BarcodeLibrary)
Plate=nan(size(BarcodeLibrary));
Row=Plate;
Column=Plate;
[r,c]=size(BarcodeLibrary);
for i=1:r
    for j=1:c
        [bc,~]=size(BarcodeLibrary{i,j});
        if bc==1
            Plate(i,j)=BarcodeLibrary{i,j}{3}(1);
            Row(i,j)=BarcodeLibrary{i,j}{3}(2);
            Column(i,j)=BarcodeLibrary{i,j}{3}(3);
        end
    end
end
        