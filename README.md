![Alt text](img/RBP_logo.png?raw=true "LOGO")

---
Updated versions of **DesignSortedLibrary.py3** and **SortedPoolCounts.py3** are available [here](https://bitbucket.org/kchuanglab/bthetacollectionpaper/src/master/ "BthetaCollectionPaper")

---


### resolve_barcode_position

This code is designed to assist in the creation of ordered barcoded transposon insertion libraries. Scripts in the repository identify barcode sequences in pools of the library,
associate barcodes with transposon insertion sites, and track the location of barcodes within the ordered library. This repository depends heavily on previously published code. Please
follow instructions for the installation and configuration of these dependencies. There are three main scripts that you will run from this repository.

1. **DesignSortedLibrary.py3**
   This script takes a *tncount* file as input from the MapTnSeq.pl script of the feba repository (1). It outputs a table associating barcodes with transposon insertion sites (*lookuptable*).
   This script is inspired by code within the feba repository (1).
   
2. **SortedPoolCounts.py3**
   This script repeatedly calls the MultiCodes.pl script from the feba repository on a set of *fastq* files from performing Bar-seq on a set of pools from the library. It
   outputs a table with the counts of each barcode in the appropriate pool (*inclusiontable*). This script is essentially a wrapper for code in the feba repository (1).
   
3. **resolve_barcode_position.m**
   This script takes the *lookuptable* and *inclusiontable* files from above. It infers the location of barcodes within the sorted library and associates each barcode with its 
   insertion sites. It outputs a table that identifies barcodes/insertions within the ordered library (*positiontable*). This script is hard-coded to analyze pool designs based on a 
   plate-row-column pooling pattern. Parts of the code dealing with barcodes that appear more than once in the library are directly inspired by source code written for the same purpose
   as part of the Knockout Sudoku analysis suite (2).
   
This code was written to resolve the barcode positions of a 40-plate library of barcoded transposon insertions in *Bacteroides thetaiotaomicron* VPI-5482. The sequencing data associated with this study is
publicly available as part of the NCBI BioProject PRJNA573294. The pre-print for a protocol paper that uses this code is currently available (3).

#### References

1. Wetmore, K.M., Price, M.N., Waters, R.J., Lamson, J.S., He, J., Hoover, C.A., Blow, M.J., Bristow, J., Butland, G., Arkin, A.P. and Deutschbauer, A., 2015. Rapid quantification of mutant fitness in diverse bacteria by sequencing randomly bar-coded transposons. *MBio*, 6(3), pp.e00306-15.

2. Baym, M., Shaket, L., Anzai, I.A., Adesina, O. and Barstow, B., 2016. Rapid construction of a whole-genome transposon insertion collection for Shewanella oneidensis by Knockout Sudoku. *Nature communications*, 7, p.13270.

3. Shiver, A.L., Culver, R., Deutschbauer, A.M., Huang, K.C. Rapid ordering of barcoded transposon insertion libraries of anaerobic bacteria *BioRxiv*, 780593



---

### Software dependencies


1. Python 3.7

       this repository has only been tested with Python 3.7
	   
	   Packages used by Python 3 scripts:
	   
       * biopython
       * pandas
       * numpy
       * argparse
       * os
       * subprocess
       * re

2. Perl 5.18.2
       
	   this repository has only been tested with Perl 5.18.2
	   
	   Packages used by Perl scripts:
	   
       * bioperl
	   
3. MATLAB 2015b *or later*

       this repository has been tested on MATLAB v. 2015b and 2018a
	   
	   Packages used by MATLAB scripts:
	   
       * Optimization Toolbox v8.1
       * Symbolic Math Toolbox v8.1
       * Statistics and Machine Learning Toolbox v11.3
	   
4. the feba code repository

       the feba code repository is available [here](https://bitbucket.org/berkeleylab/feba/src "FEBA"), please follow instructions at this link to properly download
	   and configure this code.

4. blat v. 36x2 *or later*

       blat is available [here](https://hgdownload.soe.ucsc.edu/downloads.html "blat"), please follow instructions at this site to properly download and configure this code.
	   
5. Git v. 2.15.0
	   
6. Mac OSX El Capitan *or later*

       Systems on which this code repository has been tested:
	   
       * MacBook Pro 3.1 GHz i7 processor, 16 GB memory, running Mojave (10.14.4)
       * MacBook Air 1.6 GHz i5 processor, 4 GB memory, running El Capitan (10.11.4)

---
 
### Installation and Configuration 

These instructions are for Mac OSX. The instructions assume you have downloaded git, MATLAB, Python 3, and Perl. 

1. Install blat

       1. [download](https://hgdownload.soe.ucsc.edu/downloads.html "blat") the appropriate precompiled blat from the website and move into a folder.
       2. to allow scripts from the feba repository access this code, add the following line to your .bashrc or .bash_profile file. These instructions will refer
	   to the .bashrc file.
	      
		     export PATH=$PATH:path_to_blat_repository
		  
	   3. in terminal, source the .bashrc file and check that the blat can be found and executed from the command line.
	   
		     . ~/.bashrc
		     blat
		  
		  the help menu for blat should appear.
		  
		  *See Troubleshooting for more information on configuring your .bashrc file on Mac OSX.*
		  
2. Install the bioperl module for Perl 5
			   
3. Clone the FEBA repository

       1. clone the feba repository into a new folder
	   
	         mkdir your_feba_repo
	         cd your_feba_repo
	         git clone user -your_user_name -repo online_feba_url
	   
	   2. Open .bashrc and add the following line.
	   
	         export PATH=$PATH:your_feba_repo/feba/bin/
	   
	   3. In terminal, test that the FEBA repsository is on the executable path.
	   
	         . ~/.bashrc
	         BarSeqTest.pl
	   
	      A help menu should appear.
	   
4. Install the Python 3 packages.

       1. Use pip3 to install the necessay packages by typing the following in terminal.

		     pip3 install biopython pandas numpy argparse subprocess re

5. Clone the resolve_barcode_position repository into a local folder.

       1. Clone the resolve_barcode_position into a new folder by typing the following in terminal.
	   
             mkdir your_rbp_repo
		     cd your_rbp_repo
		     git clone user -your_user_name -repo rbp_repo_url

		  
	   2. Open your .bashrc and add the following line
	   
		     export RBP=your_rbp_repo/scripts

		  
	   3. Confirm that Python 3 code from the repository can be run by typing the following in terminal.
	   
		     . ~/.bashrc
		     python3 $RBP/DesignSortedLibrary.py3
		  
		  A help menu should appear.
		  
	   4. Open your .bashrc and add the following lines.
	   
		     export MATLAB=absolute_path_to_matlab_application_bin_folder
		     alias matlab=$MATLAB/matlab
		  
		  *See Troubleshooting for more information on creating a command-line alias for MATLAB on Mac OSX*
	   5. Test that MATLAB can run on the command line.

		     matlab -nodesktop -nosplash
		    
		  A MATLAB instance should open on the command line. To exit, type the following in MATLAB.

		     exit
		 
---

### Running the examples

Execution times are for a MacBook Air running OS X El Capitan (10.11.14) with a 1.6 GHz i5 processor and 4 GB 1600 MHz DDR3 RAM.on an
The following instructions assume you have run the installation and configuration instructions above.

1. Open a terminal and navigate to the examples folder of the resolve_barcode_position repository.

	    cd $RBP
	    cd ../examples
		. ~/.bashrc
	  
2. Run MapTnSeq.pl to map RB-TnSeq reads onto the genome.

      RUNTIME: 9 min

	    MapTnSeq.pl -genome genome.fna -model modelfile -first tnseq/RBTnSeq_fastq > tncount
	  
	 *See troubleshooting section for further details on this script from the feba repository.*
	  
3. Run DesignSortedLibrary.py3 to create a lookup table associating barcode and insertion site(s).

      RUNTIME: 4 min
	  
	    python3 $RBP/DesignSortedLibrary.py3 --input tncount --genes genes --Nmin 10 --output lookuptable
      
      *See troubleshooting section for further details on this script.*
      
4. Run SortedPoolcounts.py3 to create an inclusion table listing barcode counts in each pool.

     RUNTIME: 1 h 15 min
	 
        python3 $RBP/SortedPoolCounts.py3 –k barseqkey –o inclusiontable –q 10 –f 0.01
     
     
5. Run resolve_barcode_position.m to create a position table tracking barcode location in the library.

      RUNTIME: 2 h 30 min
      
        matlab -nodesktop -nosplash -r \
        "addpath('../scripts'); \
        resolve_barcode_position(inclusiontable,lookuptable,genes,positiontable);" \
        exit;” 

---

### Troubleshooting
* Editing the .bashrc file

    For Mac OSX, the startup file used whenever a new Terminal opens is .bash_profile (in the home directory). All edits can be made to this file, or
    the following line can be added to .bash_profile to automatically source whatever configuration file you choose to create.
  
      . ~/.bashrc
	 

* Creating a MATLAB alias

    On MacOSX systems, the MATLAB App is typically found within the /Applications folder. The path to the bin folder isn't obvious in Finder, but can be worked out
    by using the autocomplete feature in Terminal to list the directory.
  
* Running MapTnSeq.pl

    MapTnSeq.pl depends on blat for mapping processed reads onto the provided genome. This code is part of the FEBA repository. Repeated executions of this script
    can give different *tncount* output files. These *tncount* files, when fed through SortedLibraryDesign.py3, can result in different barcode<->transposon insertion associations for
    a small fraction of the library (< 2% for the 40-plate *B. theta* library). Manual inspection of these differences indicate that the irreproducible insertions are in repeated elements
    in the genome, where the assignment is ambiguous. Future versions of the code for this repository will most likely re-write this script so that it records this information.
  
* Running DesignSortedLibrary.py3

    DesignSortedLibrary.py3 can exceed memory limitations when run on sets of multiple large fastq files. Future versions of the code will include a low memory usage option to handle
    these situations.
  
* Running resolve_barcode_position.m

    The long run time of this script is due to two memory-intensive steps surrounding the probabilistic prediction of barcodes which occur more than once in the library. The first step is generating all
    possible unique configurations that could explain the inclusion pattern of barcodes that occur more than once in the library. The second is fitting a Voigt distribution to the read ratio data of different
    pool combinations. These issues will be tackled in future versions of this code (see Future Direction).
  	

---

### Future Direction

1. The scripts available here were written to support the creation of an ordered barcoded transposon insertion library in the 
   anaerobic bacterium *Bacteroides thetaiotaomicron* VPI-5482. While the scripts are publicly available for use and adaptation, there
   are three limitations which may currently make a more broad adaptation of the code difficult. First, the resolve_barcode_position
   script is hard-coded to analyze a single pool design (Plate-Row-Column) and will not work for any alternative pool design. Second,
   resolve_barcode_position is written in MATLAB and depends on a number of toolboxes in order to fit a Voigt distribution to the ratio
   of pool reads. Third, the initial conditions for fitting a Voigt function to the data are hard-coded, and will need to be updated 
   within the code for every project. Work is currently underway to rewrite resolve_barcode_position in Python, improve the model fitting,
   and generalize the code to any pool design. This README page will include links to any future code releases, so stay tuned!




   
